#Dieses Programm ist nur dazu da, die Ausgaben von fnf2steps.py zu testen. Dementsprechend ist es weder besonders sorgfältig geschrieben noch irgendwie durch Kommentare erklärt. Es wird wahrscheinlich auch nie wieder benötigt oder gar verändert werden.


import turtle
import math
import time

step = 1
diagstep = math.sqrt(step*step*2)

f= open("output.txt")
turtle.pencolor("white")
print(turtle.pencolor())

for l in f:
    e,z,d = l.split(",")

    #print(e,z,d)

    if int(d) == 1:
        if turtle.pencolor() == "white":
            turtle.pencolor("red")
        elif turtle.pencolor() == "red":
            turtle.pencolor("white")

    if int(e) == 1 and int(z) == 1:
        turtle.setheading(45)
        turtle.forward(diagstep)

    elif int(e) == 0 and int(z) == 1:
        turtle.setheading(0)
        turtle.forward(step)

    elif int(e) == 1 and int(z) == 0:
        turtle.setheading(90)
        turtle.forward(step)

    elif int(e) == -1 and int(z) == -1:
        turtle.setheading(225)
        turtle.forward(diagstep)

    elif int(e) == -1 and int(z) == 0:
        turtle.setheading(270)
        turtle.forward(step)

    elif int(e) == -1 and int(z) == 1:
        turtle.setheading(315)
        turtle.forward(diagstep)

    elif int(e) == 0 and int(z) == -1:
        turtle.setheading(180)
        turtle.forward(step)

    elif int(e) == 1 and int(z) == -1:
        turtle.setheading(145)
        turtle.forward(diagstep)

    else:
        pass

turtle.done()
