# Schreibroboter

Hier findet sich eine Auflistung aller Programme in der Reihenfolhge in der man sie zum Schreiben eines mit eigenen Buchstaben erzeugten Textes verwendet:

*  [fnf2steps.py](https://gitlab.com/luca_/schreibroboter/blob/master/fnf2steps.py)             Konvertiert die fnf Dateien (in etwa vergleichbar mit svg, enthalten die Beschreibungen der zu zeichnenden Pfade), zu Anweisungen für die Schrittmotoren. 
*  [parseFNF.py](https://gitlab.com/luca_/schreibroboter/blob/master/parseFNF.py)               Wird u.a. von fnf2steps.py benötigt, um FNF Dateien verwenden zu können.


Andere Dateien:

*  [A.fnf](https://gitlab.com/luca_/schreibroboter/blob/master/A.fnf)                    Eine einfache fnf Datei die die Wege für ein A enthält, gut zum Testen.
*  [testplotter.py](https://gitlab.com/luca_/schreibroboter/blob/master/testplotter.py)           Ein Programm, dass die Ausgaben von fnf2steps.py wie später die Motoren zum Zeichnen verwendet. 